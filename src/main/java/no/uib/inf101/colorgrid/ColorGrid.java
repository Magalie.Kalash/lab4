package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  List<List<CellColor>> grid;
  CellColor cellColor;
  int rows;
  int cols;
  public ColorGrid(int rad, int kol) {
    this.rows = rad;
    this.cols = kol;
    this.grid = new ArrayList<>();
    for (int i = 0; i < rows; i++) {
      List<CellColor> row = new ArrayList<>();
      for (int j = 0; j < cols; j++) {
        row.add(new CellColor(new CellPosition(i, j), null));
      }
      this.grid.add(row);
    }
  }

  @Override
  public Color get(CellPosition pos) {
    return grid.get(pos.row()).get(pos.col()) != null ? grid.get(pos.row()).get(pos.col()).color() : null;
  }

  @Override
  public void set(CellPosition pos, Color color) throws IndexOutOfBoundsException{
    if (pos.row() > grid.size() || pos.row() < 0) {
      throw new IndexOutOfBoundsException("Position out of bounds");
    }

    List<CellColor> row = grid.get(pos.row());
    if (pos.col() > row.size() || pos.col() < 0) {
      throw new IndexOutOfBoundsException("Position out of bounds");
    } else {
      row.set(pos.col(), new CellColor(pos, color));
    }
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> all = new ArrayList<>();
    for (int i = 0; i < grid.size(); i++) {
      for (int j=0; j < grid.get(i).size(); j++) {
        CellColor col = grid.get(i).get(j);
        all.add(col);
      }
    }
    return all;
  }

  public void printcells(){
    for (int i = 0; i < grid.size(); i++) {
      for (int j=0; j < grid.get(i).size(); j++) {
        CellColor col = grid.get(i).get(j);
        System.out.println(col);
      }
    }
  }

  @Override
  public int rows() {
    return rows;
  }

  @Override
  public int cols() {
    return cols;
  }
}

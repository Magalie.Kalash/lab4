package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {
  IColorGrid iColorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
public GridView(IColorGrid iColorGrid) {
  this.iColorGrid = iColorGrid;
  this.setPreferredSize(new Dimension(400,300));
  }

  private void drawGrid(Graphics2D g2) {
    double width = this.getWidth() - OUTERMARGIN*2;
    double height = this.getHeight() - OUTERMARGIN*2;
    Rectangle2D rectangle = new Rectangle2D.Double(OUTERMARGIN,OUTERMARGIN,width,height);
    CellPositionToPixelConverter pixel = new CellPositionToPixelConverter(rectangle, iColorGrid, OUTERMARGIN);
    g2.setColor(MARGINCOLOR);
    g2.fill(rectangle);
    drawCells(g2, iColorGrid, pixel);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection collection, CellPositionToPixelConverter pixel) {
    List<CellColor> collect = collection.getCells();
    for (CellColor cell : collect) {
      CellPosition pos = cell.cellPosition();
      Rectangle2D rectangle = pixel.getBoundsForCell(pos);
      if (cell.color() != null) {
        g2.setColor(cell.color());
        g2.fill(rectangle);
      } else {
        g2.setColor(Color.DARK_GRAY);
        g2.fill(rectangle);
      }
    }
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }
}

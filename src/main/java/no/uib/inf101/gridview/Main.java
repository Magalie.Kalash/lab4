package no.uib.inf101.gridview;

import javax.swing.JFrame;
import java.awt.Color;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {
    ColorGrid colorgrid = new ColorGrid(3, 4);
    GridView gridview = new GridView(colorgrid);
    colorgrid.set(new CellPosition(0, 0), Color.RED);
    colorgrid.set(new CellPosition(0, 3), Color.BLUE);
    colorgrid.set(new CellPosition(2, 0), Color.YELLOW);
    colorgrid.set(new CellPosition(2, 3), Color.GREEN);
    JFrame frame = new JFrame("My Application");
    frame.setContentPane(gridview);
    frame.setTitle("Picture Grid");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}

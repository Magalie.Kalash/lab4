package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp) {
    double height = (box.getHeight() - (margin*(gd.rows()+1))) / gd.rows();
    double width = (box.getWidth() - (margin*(gd.cols()+1))) / gd.cols();
    double x = box.getX() + (margin * (cp.col()+1)) + (width * (cp.col()));
    double y = box.getY() + (margin * (cp.row()+1)) + (height * (cp.row()));
    return new Rectangle2D.Double(x,y,width,height);
  }

}
